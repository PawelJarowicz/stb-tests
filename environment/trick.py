import time, random
from Remote import Remote
from Resources import Resources

remote = Remote(redRatName = "RedRat-0", device = "Plato", rcuDefinitionFile = Resources().PLATO_NEW_RCU_SIGNALS_DEFINITION_FILE, applicationPATH = Resources().RED_RAT_APPLICATION, defaultSleepTime = 120)

while True:
    remote.pressButton('FAST_FORWARD')
    sleepTime = random.uniform(5.0, 20.0)
    print("Sleep time = " + str(sleepTime))
    remote.pressButton('PLAY')
    sleepTime = random.uniform(15.0, 30.0)
    print("Sleep time = " + str(sleepTime))
    remote.pressButton('REWIND')
    sleepTime = random.uniform(5.0, 20.0)
    print("Sleep time = " + str(sleepTime))