import  os, sys, subprocess, time
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../'))
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../..'))

class Remote(object):
    
    button_dict = {
        '0':'0',
        '1':'1',
        '2':'2',
        '3':'3',
        '4':'4',
        '5':'5',
        '6':'6',
        '7':'7',
        '8':'8',
        '9':'9',
        'GAMES':'GAMES',
        'ARROW_DOWN':'ARROW_DOWN',
        'ARROW_LEFT':'ARROW_LEFT',
        'ARROW_RIGHT':'ARROW_RIGHT',
        'ARROW_UP':'ARROW_UP',
        'BLUE':'LANGUAGE',
        'BOOKMARK':'BOOKMARK',
        'CHANNEL_DOWN':'CHANNEL_DOWN',
        'CHANNEL_UP':'CHANNEL_UP',
        'EXIT':'EXIT',
        'EXTRA':'EXTRA',
        'FAST_FORWARD':'FAST_FORWARD',
        'GREEN':'HELP',
        'HASH':'EXTRA',
        'HELP':'HELP',
        'INFO':'INFO',
        'LANGUAGE':'LANGUAGE',
        'MENU':'MENU',
        'MUTE':'MUTE',
        'OK':'OK',
        'ON_DEMAND':'ON_DEMAND',
        'PLAY':'PLAY',
        'PAUSE':'PAUSE',
        'PLAYLIST':'PLAYLIST',
        'POWER':'POWER',
        'RADIO':'RADIO',
        'RECORD':'RECORD',
        'RED':'BOOKMARK',
        'REWIND':'REWIND',
        'STOP':'STOP',
        'SUBTITLES':'SUBTITLES',
        'TV':'TV',
        'TV_GUIDE':'TV_GUIDE',
        'TXT':'TXT',
        'VOLUME_DOWN':'VOLUME_DOWN',
        'VOLUME_UP':'VOLUME_UP',
        'YELLOW':'SUBTITLES'
    }
    
    def __init__(self, redRatName, device, rcuDefinitionFile, applicationPATH, defaultSleepTime):
        self.redRatName = redRatName
        self.device = device
        self.rcuDefinitionFile = rcuDefinitionFile
        self.applicationPATH = applicationPATH
        self.defaultSleepTime = defaultSleepTime
        if not os.path.isfile(self.applicationPATH):
            print self.applicationPATH
            raise IOError(os.path.abspath(self.applicationPATH))
        if not os.path.isfile(rcuDefinitionFile):
            raise IOError(os.path.abspath(self.rcuDefinitionFile))

    def pressButton(self, buttonKey):
        signal = self.__getSignal(buttonKey)
        command = self.applicationPATH + ' ' + self.redRatName + ' ' + '-output' + ' ' + self.rcuDefinitionFile + ' ' + '-device' + ' ' + self.device + ' ' + '-signal' + ' ' + signal
        print command
        self.__sendCommand(command)
        time.sleep(self.defaultSleepTime)
            
    def __getSignal(self, buttonKey):
        if (self.button_dict.keys().count(buttonKey) < 1):
            raise TypeError(buttonKey)
         
        return self.button_dict[buttonKey]

    def __sendCommand(self, command):
        rcu = subprocess.Popen(command, shell=True, stderr=subprocess.PIPE)
        while True:
            out = rcu.stderr.read(1)
            if out == '' and rcu.poll() != None:
                break
        if out != '':
            sys.stdout.write(out)
            sys.stdout.flush()
        print "Signal sent"

#if __name__ == '__main__':
    #from environment.Resources import Resources
    #remote = Remote(redRatName = "RedRat-2", device = "Plato", rcuDefinitionFile = Resources().PLATO_RCU_SIGNALS_DEFINITION_FILE, applicationPATH = Resources().RED_RAT_APPLICATION)
    #remote.pressButton('MENU')