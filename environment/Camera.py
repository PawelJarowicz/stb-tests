import os, sys, re, cv2
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../'))
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../..'))

class Camera(object):
    __filters = ['COLOR','GREYSCALE','BW']
    __defResolution = [1920,1080]
    
    @staticmethod
    def quickScreenshot(coordinates):
        screenshot= Camera.captureScreenshot()
        tresholdedScreenshot = Camera().filterImage(screenshot, 'BW')
        croppedTresholdedScreenshot = Camera().cropImage(tresholdedScreenshot, coordinates)
        return tresholdedScreenshot, croppedTresholdedScreenshot

    @staticmethod
    def captureScreenshot():
        camera = cv2.VideoCapture(0)   # cv2 doesn't support any other video captures
        camera.set(3,Camera.__defResolution[0])
        camera.set(4,Camera.__defResolution[1])
        
        retryAttempts = 5
        
        print "Capturing screenshot..."
        #Capturing frame
        # retry attempts incremented, because of python 'for' loop construction
        retryAttempts=retryAttempts+1
        for i in range(1,retryAttempts):
            ret, frame = camera.read()
            # ret - bool if frame is read correctly
            if(ret):
                print "Capture successful."
                #releasing capture
                camera.release()
                cv2.destroyAllWindows()
                return frame
            elif i == retryAttempts:
                #releasing capture
                camera.release()
                cv2.destroyAllWindows()
                print "Capture failed! ("+i+"\/"+retryAttempts+")"
                raise CaptureError('Cannot capture image.')

    @staticmethod
    def filterImage(image, filter):
        if filter in Camera.__filters:
            if filter == 'COLOR':
                filteredImage = cv2.cvtColor(image,cv2.COLOR_BGR2RGB)
            elif filter == 'GREYSCALE' or filter == 'BW':
                filteredImage = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                if filter == 'BW':
                    (thresh, filteredImage) = cv2.threshold(filteredImage, 128, 255, cv2.THRESH_BINARY)
        else:
            raise TypeError('Unsupported filter! (filter not found or wrong name of filter).')
        print "Image filtered successfully!"
        return filteredImage
    
    @staticmethod
    def saveImage(saveFile, destination):
        print destination
        parentDirectory = os.path.abspath(os.path.join(destination, os.pardir))
        print parentDirectory
        if not os.path.exists(parentDirectory):
            os.makedirs(parentDirectory)
        if not(re.match('.*\.\w{3}$', destination)):
            destination = destination + '.png'
        cv2.imwrite(destination, saveFile)
        print "Picture saved:\n"+os.path.abspath(destination)
    
    @staticmethod
    def cropImage(image, coordinates):
        return image[coordinates[1]:coordinates[3],coordinates[0]:coordinates[2]]
        
class CaptureError(Exception):
    def __init__(self, message):
        self.message = message
        
    def __str__(self):
        return str(self.message)

if __name__ == "__main__":
    
    screenshot= Camera.captureScreenshot()
    colorScreenshot = Camera.filterImage(screenshot, 'COLOR')
    tresholdScreenshot = Camera.filterImage(screenshot, 'BW')
    greyscaleScreenshot = Camera.filterImage(screenshot, 'GREYSCALE')
    if len(sys.argv) == 5:
        colorScreenshot = cropImage(colorScreenshot, sys.argv[1:])
        tresholdScreenshot = cropImage(tresholdScreenshot, sys.argv[1:])
        greyscaleScreenshot = cropImage(greyscaleScreenshot, sys.argv[1:])
    Camera.saveImage(colorScreenshot, 'c')
    Camera.saveImage(tresholdScreenshot, 't')
    Camera.saveImage(greyscaleScreenshot, 'g')
    
