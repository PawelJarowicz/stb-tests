import time, random
from Remote import Remote
from Resources import Resources

remote = Remote(redRatName = "RedRat-1", device = "Plato", rcuDefinitionFile = Resources().PLATO_NEW_RCU_SIGNALS_DEFINITION_FILE, applicationPATH = Resources().RED_RAT_APPLICATION, defaultSleepTime = 0)

def randomButton():
    button_index = random.randrange(0, len(Remote.button_dict))
    button_name = Remote.button_dict[Remote.button_dict.keys()[button_index]]
    return button_name

while True:
    button_name = randomButton()
    if button_name != "POWER":
        remote.pressButton(button_name)
        print("Pressed button: " + button_name)
        sleep_time = random.uniform(1.0, 10.0)
        print("Sleep time = " + str(sleep_time))
        time.sleep(sleep_time)
