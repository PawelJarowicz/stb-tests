import os

class Resources():
    PROJECT_DIR = os.path.abspath(os.path.join(os.getcwd(), os.pardir)) + '\\'
    SCREENSHOTS_DIR = PROJECT_DIR + 'screenshots\\'
    SCREENSHOTS_FULL_DIR = PROJECT_DIR + 'screenshots_fullscreen\\'
    TEMPLATES_DIR = PROJECT_DIR + 'templatePictures\\'
    LIVE_TV_TEMPLATES_DIR = TEMPLATES_DIR + 'liveTV\\'
    MAIN_MENU_TEMPLATES_DIR = TEMPLATES_DIR + 'mainMenu\\'
    BOUQUETS_MENU_TEMPLATES_DIR = TEMPLATES_DIR + 'bouquetsMenu\\'
    INFO_BANNER_TEMPLATES_DIR = TEMPLATES_DIR + 'infoBanner\\'
    NUMBERS_TEMPLATES_DIR = TEMPLATES_DIR + 'numbers\\'
    SETTINGS_TEMPLATES_DIR = MAIN_MENU_TEMPLATES_DIR + 'settings\\'
    ADVANCED_SETTINGS_TEMPLATES_DIR = SETTINGS_TEMPLATES_DIR + 'advancedSettings\\'
    VIDEO_SETTINGS_TEMPLATES_DIR = SETTINGS_TEMPLATES_DIR + 'video\\'
    HELP_TEMPLATE_DIR = TEMPLATES_DIR + 'help\\'
    PLATO_RCU_SIGNALS_DEFINITION_FILE = PROJECT_DIR + 'src\\Plato_RCU.xml'
    PLATO_NEW_RCU_SIGNALS_DEFINITION_FILE = PROJECT_DIR + 'src\\Plato_New_RCU.xml'
    RED_RAT_APPLICATION = PROJECT_DIR + 'src\\RedRat\\RR_CLI.exe'