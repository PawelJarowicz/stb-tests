import time, random
from Remote import Remote
from Resources import Resources

remote = Remote(redRatName = "RedRat-0", device = "Plato", rcuDefinitionFile = Resources().PLATO_NEW_RCU_SIGNALS_DEFINITION_FILE, applicationPATH = Resources().RED_RAT_APPLICATION, defaultSleepTime = 0)

while True:
    sleepTime = random.uniform(9.0, 11.0)
    print("Sleep time = " + str(sleepTime))
    remote.pressButton('CHANNEL_UP')
    time.sleep(1)
    remote.pressButton('EXIT')
    time.sleep(sleepTime)
    