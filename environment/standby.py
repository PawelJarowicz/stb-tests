from Remote import Remote
from Resources import Resources

remote = Remote(redRatName = "RedRat-0", device = "Plato", rcuDefinitionFile = Resources().PLATO_NEW_RCU_SIGNALS_DEFINITION_FILE, applicationPATH = Resources().RED_RAT_APPLICATION, defaultSleepTime = 120)

while True:
    remote.pressButton('POWER')