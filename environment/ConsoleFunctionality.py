""" Version 1.1 """

import os, serial, time, datetime, sys, threading, re, string, subprocess
from Queue import Queue

sys.path.append(os.path.abspath(os.path.dirname(__file__)))

ser = serial.Serial('COM10', 115200)
testDirectory = "TestResults/" + datetime.datetime.now().strftime("%Y-%m-%d--%H-%M-%S-%f")
logsFileLock = threading.Lock()     # This variable lock access to file with logs
toFileQueue = Queue()     # Queue with logs to save to file
writeTop = 0
# While "True" all threads are working
runAllThreads = True
perfCheck = False
# list of keyword for SearchInFileThread. If found keyowrd from this list then test should be stoped
errorsList = ["segmentation fault", "critical", "assert", "kernel panic", "assertion failed", "callstack", "call stack"]

"""================================================= ConsoleReadThread class ================================================="""

# This thread read console, print Console logs in Terminal and puts logs to queue to save to file
class ConsoleReadThread(threading.Thread):
    printT = True   # if True Thread print logs on Terminal
    saveF = True    #if True Thread save logs to file

    # 'Constructor'
    def __init__(self, toFileQueue = toFileQueue, printOnTerminal=True, saveToFile=True, performanceCheck=True, numOfSecToCheckLoad = 10):
        global perfCheck
        threading.Thread.__init__(self, name = 'ConsoleRead')
        self.toFileQueue = toFileQueue
        self.printT = printOnTerminal
        self.saveF = saveToFile
        perfCheck = performanceCheck
        self.numOfSec = numOfSecToCheckLoad

    # Method that runs while runAllThreads is True and read Console logs
    def run(self):
        global runAllThreads
        global writeTop
        global perfCheck
        
        while runAllThreads:
            serialLine = ser.readline()
            if writeTop >= self.numOfSec and perfCheck:
                ser.write("top -bn1\n")
                writeTop = 0
            if self.printT:
                sys.stdout.write(datetime.datetime.now().strftime("[%H:%M:%S] ") + serialLine)
            if self.saveF:
                toFileQueue.put(serialLine) # put readed logs to Queue. toFileQueue is readed by SavetoFileThread
                
    def writeOnConsole(self, toWrite=""):
        ser.write(toWrite + "\n")

"""================================================= SaveToFileThread class ================================================="""

# Thread saveing logs from console to file
class SaveToFileThread(threading.Thread):

    # 'Constructor'
    def __init__(self, toFileQueue = toFileQueue, TestDirectory = ""):
        threading.Thread.__init__(self, name = 'SaveToFile')
        self.toFileQueue = toFileQueue
        self.TestDirectory = TestDirectory
        day = datetime.datetime.now().strftime("%Y-%m-%d--%H-%M-%S-%f")

        # lock access to File and write in 
        with logsFileLock:
            try:
                logsFile = open(self.TestDirectory + '/logs.txt', 'w')
                logsFile.write(day)
                cpuFile = open(self.TestDirectory + '/cpu.txt', 'w')
                cpuFile.write(day)
            finally:
                logsFile.close()

    # Method that runs while runAllThreads is True and wait for logs to save, if log is in Queue then is saved
    def run(self):
        global runAllThreads
        global logsFileLock
        
        while runAllThreads:
            # if queue is NOT empty then write logs to file
            if not toFileQueue.empty():
                # using lock for safe access to file
                with logsFileLock:
                    try:
                        logFile = open(self.TestDirectory + '/logs.txt', 'a')
                        logFile.write(datetime.datetime.now().strftime("[%H:%M:%S] ") + toFileQueue.get())
                    finally:
                        logFile.close()

"""================================================= SearchInFileThread class ================================================="""

# Thread saveing logs from console to file
class SearchInFileThread(threading.Thread):

    keywords = []
    position = 0
    allResults = []

    # Constructor. *args is args for functionIfKeyword
    def __init__(self, TestDirectory, keywords, functionIfKeyword, acceptKeywordArgument = True, *args):
        global perfCheck
        self.TestDirectory = TestDirectory
        threading.Thread.__init__(self, name = 'SearchInFile')
        self.keywords = keywords
        if perfCheck:    
            cpuLogs = open(self.TestDirectory + '/cpu.txt', 'w')
            cpuLogs.close()
            memLogs = open(self.TestDirectory + '/mem.txt', 'w')
            memLogs.close()
        else:
            self.keywords = keywords
        self.position = 0
        self.func = functionIfKeyword
        self.argList = args
        self.TestDirectory = TestDirectory
        self.acceptKeywordArgument = acceptKeywordArgument  # if this variable is True then found keyword is forwarded to function (functionIfKeyword)

    # Method that runs while runAllThreads is True and search for keyword in file
    def run(self):
        global runAllThreads
        global logsFileLock
        global cpuFileLock
        global writeTop
        global perfCheck
        global perList
        
        keywordForFunc = None
        while runAllThreads:
            writeTop = writeTop + 1
            text = ""
            runF = False
            # read file with logsFileLock for safe reading file
            with logsFileLock:
                logFile = open(self.TestDirectory + '/logs.txt', 'r')   #open file to read
                try:
                    logFile.seek(self.position)     #read file from previous possition
                    for line in logFile:        #reading to end of file
                        if perfCheck:
                            cpuMatch = re.search(r'cpu:\s{1,2}(\d+.\d)', string.lower(line))
                            if cpuMatch is not None:
                                cpuLog = cpuMatch.group(1)
                                #print "CPU: " + cpuLog + "%"
                                cpuLog = cpuLog + "\n"
                                cpuFile = open(self.TestDirectory + '/cpu.txt', 'a')
                                cpuFile.write(cpuLog)
                                cpuFile.close()
                            memMatch = re.search(r'mem:\s{1,2}(\d+)', string.lower(line))
                            if memMatch is not None:
                                memUsage = memMatch.group(1)
                                #print "Mem: " + memUsage + "K"
                                memUsage = memUsage + "\n"
                                cpuFile = open(self.TestDirectory + '/mem.txt', 'a')
                                cpuFile.write(memUsage)
                                cpuFile.close()
                        for keyword in self.keywords:       #check all keywords from list
                            matchObj = re.search(string.lower(keyword), string.lower(line))
                            if matchObj is not None:    #if found keyword
                                self.allResults.append(matchObj)
                    self.position = logFile.tell()      #saving possition of reading karete
                finally:
                    logFile.close()
                for res in self.allResults:     # if is somethig in allResults then:
                    keywordForFunc = res.group()
                    runF = True     # set function to run
                    runAllThreads = False       # and stop work of all Threads
                self.allResults = []
            if runF:        # if True run functionIfKeyword
                if self.acceptKeywordArgument:      # if function accept keyword as parameter
                    self.func(keywordForFunc, *self.argList)    # run it with found keyword as parameter
                else:       # or without keyword
                    self.func(*self.argList)
            time.sleep(1)   # reading file every second
            
"""=================================================== PlotGenerator class ===================================================="""
            
class PlotGenerator():
    def generatePlot(self, tDirectory = "", fileToPlotList = ["\\cpu.txt", "\\mem.txt"]):
        
        scriptFile = open(tDirectory + '\\script.dem', 'w')
        scriptFile.write('# Script to generate png file with plot.')
        scriptFile.close()
        scriptFile = open(tDirectory + '\\script.dem', 'a')
        scriptFile.write("#set terminal png \n")
        scriptFile.write("set terminal pngcairo \n")
        for fl in fileToPlotList:
            scriptFile.write("set output '" + tDirectory + "\\" + fl[1:4] + ".png' \n")
            scriptFile.write('set title "' + fl[0:4] + '"\n')
            scriptFile.write("unset key \n")
            scriptFile.write("set style data linespoints \n")
            if fl[1:4] == 'mem':
                scriptFile.write("set yrange [*<100900:101300<*]\n")
            elif fl[1:4] == 'cpu':
                scriptFile.write("set yrange [0:100]\n")
            strToWrite = "'" + tDirectory + fl + "'"
            scriptFile.write("plot " + strToWrite + "\n")
            scriptFile.write("#replot\n")
        scriptFile.write("exit")
        scriptFile.close()
        scrF = "gnuplot " + tDirectory + "\\script.dem"
        p = subprocess.Popen(scrF, shell = True)
        
        return True

            
            
            
            