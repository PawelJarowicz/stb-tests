import os, sys, cv2, time
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../'))
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../..'))
from environment.Remote import Remote
from environment.Resources import Resources
from environment.Camera import Camera

class Plato(object):

    remote = Remote(redRatName = "RedRat-3", device = "Plato", rcuDefinitionFile = Resources().PLATO_NEW_RCU_SIGNALS_DEFINITION_FILE, applicationPATH = Resources().RED_RAT_APPLICATION, defaultSleepTime = 3)
        
    @staticmethod
    def checkIfImagesSimilar(capture, template, tolerance):
        print 'Checking if images are similar, given the tolerance'
        template = cv2.imread(template,0)
        if template is None:
            raise IOError('Template file not found!')
        if (capture.shape[:2] != template.shape[:2]):
            raise IndexError('Images have different sizes!')
        height, width = capture.shape[:2]
        tolerancePx = int(height*width*tolerance)
        print 'Tolerance: %0.2f\ntolerancePx: %d' % (tolerance,tolerancePx)
        
        for x in range(0, height):
            for y in range(0, width):
                if capture[x,y] != template[x,y]:
                    tolerancePx=tolerancePx-1
                    if tolerancePx==0:
                        return False
        return True

    @staticmethod
    def checkImagesSimilarity(capture, template):
        print 'Checking images similarity'
        template = cv2.imread(template,0)
        if template is None:
            raise IOError('Template file not found!')
        if (capture.shape[:2] != template.shape[:2]):
            raise IndexError('Images have different sizes!')

        similarPxNumber = 0        
        height, width = capture.shape[:2]
        size = height*width

        for x in range(0, height):
            for y in range(0, width):
                if capture[x,y] == template[x,y]:
                    similarPxNumber = similarPxNumber + 1
                    result = float(similarPxNumber)/float(size)
        print 'Images similarity: %0d/%0d (%0.2f)' %(similarPxNumber, size, result)
        return result

    @staticmethod
    def cropImage(imageFile, coordinates):
        if not os.path.isfile(imageFile):
            raise IOError(os.path.abspath(imageFile))
        else:
            image = Image.open(imageFile)
            print 'Image cropped.'
            return image.crop(coordinates)
        
    @staticmethod
    def quickCroppSave(destinationPath, coordinates):
        screenshot                  = Camera().captureScreenshot()
        tresholdedScreenshot        = Camera().filterImage(screenshot, 'BW')
        croppedTresholdedScreenshot = Plato.cropImage(tresholdedScreenshot, coordinates)
        Camera().saveImage(croppedTresholdedScreenshot, destinationPath)