*** Settings ***
Suite Setup       exitToLiveTV    # Resets box to LiveTV before test suite
Test Teardown     exitToLiveTV    # Resets box to LiveTV after each test
Library           Collections
Library           ./../core/mainMenu/Settings.py
Library           ./../core/MainMenu.py
Library           ./../core/mainMenu/Help.py
Library           ./../core/infoBanner/InfoBanner.py
Library           ./../core/mainMenu/help/TVSignal.py
Library           ./../core/LiveTV.py
Library           ./../core/BouquetsMenu.py
Library           ./../core/ParentMenu.py
*** Variables ***
${RETRIES}    3
*** Test Cases ***
OpenMainMenu
    [Tags]    smoke    mainMenu
    ${mainMenu}=    openMainMenu
    closeMenu    ${mainMenu}

OpenInfoBanner
    [Tags]    smoke  infoBanner
    openInfoBanner
    Sleep    8s

OpenBouquetsMenu
    [Tags]    smoke  bouquets
    ${bouquetsMenu}=    openBoquetsMenu
    closeMenu    ${bouquetsMenu}

#CheckChannelInBouquetsMenu
#    [Tags]     bouquets
#    [Documentation]
#    ...    Channel to check: 901 (Playboy), because it appears in middle of bouquets list.
#    ...    1) switch to chanel
#    ...    2) open bouquets menu & check if bouquetsMenu opens properly
#    ...    3) check if bouquet name is 'NOVA'
#    ...    4) check if selected row is 6
#    ...    5) check if displayed channel number is 901

#    ${expected_channel_Nr}=     Set Variable    901
#    ${expected_Bouquet_Name}=   Set Variable    NOVA
#    ${expected_row_Nr}=         Set Variable    6

#    switchChannel    ${expectedChannelNr}
#    Sleep    30s
#    ${bouquetsMenu}=  openBoquetsMenu
#    ${bouquetName}=  getOpenedBouquetName
#    Should Be Equal As Strings  ${expected_Bouquet_Name}  ${bouquetName}
#    ${row}=  Call Method  ${bouquetsMenu}  getSelectedRow
#    Should Be Equal As Numbers  ${expectedRowNr}  ${row}
#    ${channelNr}=  Call Method  ${bouquetsMenu}  getChannelNumber  ${row}
#    Should Be Equal As Numbers  ${expectedChannelNr}  ${channelNr}

#ChangeChannelByBouquetsMenu
#    [Tags]     bouquets
#    [Documentation]
#    ...    Changes channel to one on different boquet.
#    ...    1) open bouquets menu & check if bouquetsMenu opens properly
#    ...    2) check bouquet name
#    ...    3) switch to next bouquet
#    ...    4) check again bouquet name
#    ...    5) check if bouquet name changed
#    ...    6) navigate three rows down
#    ...    7) check channel number
#    ...    8) confirm choice
#    ...    9) check if channel is changed to selected one

#    ${bouquetsMenu}=  openBoquetsMenu
#    ${bouquetName}=  getOpenedBouquetName
#    switchToNextBouquet  ${bouquetsMenu}
#    Sleep  2s
#    ${nextBouquetName}=  getOpenedBouquetName
#    Should Not Be Equal As Strings  ${bouquetName}  ${nextBouquetName}
#    : FOR    ${INDEX}  IN RANGE  1  4
#    \    Call Method  ${bouquetsMenu}  down
#    ${row}=  Call Method  ${bouquetsMenu}  getSelectedRow
#    ${channelNr}=  Call Method  ${bouquetsMenu}  getChannelNumber  ${row}
#    Call Method  ${bouquetsMenu}  confirmSelection
#    Should Not Be Equal As Strings  ${channelNr}  'None'
#    # well, checking channel is not implemented yet

NavigateThroughMainMenu
    ${mainMenu}=           openMainMenu
    navigateThroughMenu    ${mainMenu}

NavigateThroughSettings
    ${mainMenu}=    openMainMenu
    ${settingsMenu}=    chooseFromMenu    ${mainMenu}    Settings
    navigateThroughMenu    ${settingsMenu}

OpenTVSignalMenu
    ${helpMenu}=    navigateToHelpMenu
    chooseFromMenu    ${helpMenu}    TVSignal

NavigateThroughHelp
    ${helpMenu}=    navigateToHelpMenu
    navigateThroughMenu    ${helpMenu}

ScanSAT
    ${scanningMenu}=    navigateToScanMenu
    Call Method    ${scanningMenu}    openElement    SAT
    Wait Until Keyword Succeeds    5min    5sec    checkIfScanningComplete    ${scanningMenu}
    Call Method    ${scanningMenu}    exit

ScanDTT
    ${scanningMenu}=    navigateToScanMenu
    Call Method    ${scanningMenu}    openElement    SATandDTT
    Wait Until Keyword Succeeds    5min    5sec    checkIfScanningComplete    ${scanningMenu}
    Call Method    ${scanningMenu}    exit

*** Keywords ***
openInfoBanner
    ${infoBanner}=  openMenu  InfoBanner   ${RETRIES}
    [Return]  ${infoBanner}

openBoquetsMenu
    ${boquetsMenu}=  openMenu  BouquetsMenu   ${RETRIES}
    [Return]  ${boquetsMenu}

openMainMenu
    ${mainMenu}=  openMenu    MainMenu   ${RETRIES}
    [Return]  ${mainMenu}

openMenu
    [Arguments]    ${menuClassName}    ${RETIRES}
    ${menuInstance}=    Get Library Instance    ${menuClassName}
    :FOR  ${INDEX}    IN RANGE    0    ${RETRIES}
    \    Call Method  ${menuInstance}  open
    \    ${result}=    Wait Until Keyword Succeeds    15sec    2sec    Call Method    ${menuInstance}    isOpened
    \    Run Keyword If    '${result}' == 'True'    Exit For Loop
    Run Keyword If    '${result}' == 'False'    Fail    Cannot open ${menuClassName}!
    [Return]    ${menuInstance}

closeMenu
    [Arguments]    ${menuInstance}
    :FOR  ${INDEX}    IN RANGE    0    ${RETRIES}
    \    Call Method  ${menuInstance}  close
    \    ${result}=    Wait Until Keyword Succeeds    15sec    2sec    Call Method    ${menuInstance}    isOpened
    \    Run Keyword If    '${result}' == 'False'    Exit For Loop
    Run Keyword If    '${result}' == 'True'    Fail    Cannot open ${menuClassName}!

switchChannel
    [Arguments]    ${channelNr}
    LiveTV.Switch Channel    ${channelNr}

switchToNextBouquet
    [Arguments]    ${bouquetsMenu}
    Call Method  ${bouquetsMenu}  next
    
switchToPreviousBouquet
    [Arguments]    ${bouquetsMenu}
    Call Method  ${bouquetsMenu}  previous

exitToLiveTV
    ParentMenu.exitToLiveTV

navigateToScanMenu
    ${mainMenu}=    openMainMenu
    ${settingsMenu}=    chooseFromMenu    ${mainMenu}    Settings
    ${advSettingsMenu}=    chooseFromMenu    ${settingsMenu}    AdvancedSettings
    ${scanningMenu}=    chooseFromMenu    ${advSettingsMenu}    Scanning
    [Return]    ${scanningMenu}
    
navigateToHelpMenu
    ${mainMenu}=    openMainMenu
    ${helpMenu}=    chooseFromMenu    ${mainMenu}    Help
    [Return]    ${helpMenu}

checkIfScanningComplete
    [Arguments]    ${scanningMenu}
    ${isComplete}=    Call Method    ${scanning Menu}    isScanComplete
    ${result}=    Should Be True    ${isComplete}
    [Return]    ${result}

chooseFromMenu
    [Arguments]    ${menu}    ${element}
    ${submenu}=    Call Method    ${menu}    openElement    ${element}
    ${isOpened}=    Call Method    ${submenu}    isOpened
    Should Be True    ${isOpened}    ${submenu} is not opened!
    [Return]    ${submenu}

navigateThroughMenu
    [Arguments]    ${menu}
    @{elementsList}=    Call method    ${menu}    getElementsList
    : FOR    ${element}    IN    @{elementsList}
    \    Call Method    ${menu}    selectElement    ${element}
    \    ${isSelected} =    Call Method    ${menu}    isSelected    ${element}
    \    Should Be True    ${isSelected}    ${element} is not Selected!

isTVSignalOpened
    @{elementsList} =    TVSignal.getElementsList
    : FOR    ${element}    IN    @{elementsList}
    \    TVSignal.confirmChannel    ${element}
    \    ${isOpened}=    TVSignal.isOpened
    \    ShouldBeTrue    ${isOpened}    Element is not opened

navigateBouquets
    @{elemenstList}=    Bouquets.getBouquetsList
    : FOR    ${element}    IN    @{elemenstList}
    \    LiveTV.selectPreviousBouquet
    \    scrollBouquetsElement

scrollBouquetsElement
    : FOR    ${index}    IN RANGE    0    5
    \    LiveTV.navigateServiceList    DOWN
    sleep    2s
