*** Settings ***
Suite Setup       MainMenu.exitToLiveTV    # Resets box to LiveTV before test suite
Test TearDown     Run Keyword If Test Failed    CheckNoSignal
Library           Collections
Library           ./../core/mainMenu/Settings.py
Library           ./../core/MainMenu.py
Library           ./../core/mainMenu/Help.py
Library           ./../core/infoBanner/InfoBanner.py
Library           ./../core/mainMenu/help/TVSignal.py
Library           ./../core/LiveTV.py
Library           ./../core/BouquetsMenu.py
Library           ./../core/ParentMenu.py
*** Variables ***
${noTVSignalsCounter}    None
*** Test Cases ***
CheckNoTVSignal
	CheckNoSignal
*** Keywords ***
ZapChannels
    ${liveTV}=     LiveTV.getThis
    :FOR   ${i}    IN RANGE    100
    \    LiveTV.channelPlus
    \    ${isNoTVSignal}=    Call Method    ${liveTV}    isNoTVSignalShown
    \    Should Not Be True    ${isNoTVSignal}    No TV Signal!
    \    Sleep    5s
    
CheckNoSignal
	:FOR   ${i}    IN RANGE    100
    \    ZapChannels