"""Starting script for Plato Automation Tests

Script runs .robot test suite. After suite is finish, it saves and sends email with Robot Framework generated raports.

Use: python __init__.py [options] [source]

Options:
  -b, --build             Jenkins build information
  -e, --email             recipient of an email with generated reports
  -h, --h                 shows this help message
  -i                      input Robot Framework file with test suite to run
  -r  --repeats           suite run repeats amount

Examples of use:
  __init__.py
  __init__.py -i suite.robot
  __init__.py -i suite.robot -e mymail@mail.com
  
Starting script was created by: Hubert Trela
"""

import os, sys, re, smtplib
from getopt import getopt, GetoptError
from datetime import datetime
import subprocess
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email import Encoders
from email.utils import COMMASPACE, formatdate

current_time        = datetime.now().strftime('%y-%m-%d_%H.%M.%S')
logs_directory      = os.getcwd()+'//'+'report__'+current_time+')'
mail_from_login     = 'test.automation@altimi.com'
mail_from_password  = '>r";kTee9hMO'
mail_title          = 'PLATO TEST REPORT ('+current_time+')'
mail_description    = 'Automation report test mail.'
files = [logs_directory+'//report.html',logs_directory+'//log.html',logs_directory+'//output.xml']
    
def send_mail(send_from, recipients, subject, text, files=[], username='', password='', isTls=True):
    msg = MIMEMultipart()
    msg['From'] = send_from
    msg['To'] = ", ".join(recipients)
    msg['Date'] = formatdate(localtime = True)
    msg['Subject'] = subject

    msg.attach( MIMEText(text) )
    
    for file in files:
        if os.path.isfile(file):
            part = MIMEBase('application', 'octet-stream')
            part.set_payload(open(file, 'rb').read())
            Encoders.encode_base64(part)
            part.add_header('Content-Disposition',
               'attachment; filename="%s"' % os.path.basename(file))
            msg.attach(part)

    smtp = smtplib.SMTP("mail.altimi.com", 25)
    if isTls: smtp.starttls()
    smtp.login(username, password)
    smtp.sendmail(send_from, recipients, msg.as_string())
    smtp.quit()
        
def main(argv):
    
    input_file = os.getcwd()+'\PlatoAutomationSuite.robot'
    recipients = ['pajaro@softiti.com','krzysztof.skrypnik@softiti.com']
    #recipients = ['krzysztof.skrypnik@softiti.com']
    build      = ''
        
    try:
        opts, args = getopt(argv, "hi:e:", ["help", "email="])
    except GetoptError:
        usage()
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-b", "--build"):
            build = "(build: "+build+")"
        elif opt in ("-e", "--email"):
            recipients = arg
        elif opt in ("-h", "--help"):
            usage()
            sys.exit(2)
        elif opt == '-i':
            input_file = arg
        elif opt in ("-r", "--repeats"):
            repeats == arg
                
    source = "".join(args)
        
    os.mkdir(logs_directory);
    output = ""
    
    #for i in range(0, 10):
    for i in range(0, 1):    
        p = subprocess.Popen(['C:/Python27/python.exe', '-m', 'robot', '--loglevel', 'DEBUG', '-d', logs_directory, input_file], stdout=subprocess.PIPE)
        for line in iter(p.stdout.readline, ''):
            sys.stdout.write(line)
            output +=line

        test_results = re.search('(\d+)\stest\w?\stotal,\s(\d+)\spassed,\s(\d+)\sfailed', output)
        mail_title = "-PLATO REPORT- All/Passed/Failed: "+test_results.group(1)+'/'+test_results.group(2)+'/'+test_results.group(3)+build
        send_mail(mail_from_login, recipients, mail_title, mail_description, files, mail_from_login, mail_from_password)

def usage():
    print __doc__
    
if __name__ == "__main__":
    main(sys.argv[1:])