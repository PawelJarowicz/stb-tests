import os, sys
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../'))
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../..'))
from Menu import Menu
from core.LiveTV import LiveTV
from mainMenu.TVGuide import TVGuide
from mainMenu.SearchTitle import SearchTitle
from mainMenu.OnDemand import OnDemand
from mainMenu.Planner import Planner
from mainMenu.Messages import Messages
from mainMenu.Settings import Settings
from mainMenu.Help import Help
from environment.Plato import Plato
from environment.Resources import Resources
from core.ParentMenu import ParentMenu
    
class MainMenu(Menu):

    def __init__(self):
        super(MainMenu, self).__init__()
        self.elementsList = ['LiveTV', 'TVGuide', 'Search', 'OnDemand', 'Planner', 'Messages', 'Settings', 'Help']
        self.setSelectedElement(self.elementsList[0])
        self.coordinatesDictionary = {'LiveTV': (120,225,345,280), 'TVGuide': (120,285,345,340), 'Search': (120,360,345,415), 'OnDemand': (120,435,345,490), 'Planner': (120,510,345,565), 'Messages': (120,585,345,640), 'Settings': (120,660,345,715), 'Help': (120,735,345,790)}
        self.templatePicturesPath = Resources().MAIN_MENU_TEMPLATES_DIR
    
    def open(self):
        self.exitToLiveTV()
        self.remote.pressButton('MENU')

    def close(self):
        self.remote.pressButton('EXIT')
    
    def openSelected(self):
        self.remote.pressButton('OK')
        
        if self.selectedElement == 'LiveTV':
            return LiveTV()
        elif self.selectedElement == 'TVGuide':
            return TVGuide()
        elif self.selectedElement == 'Search':
            return SearchTitle()
        elif self.selectedElement == 'OnDemand':
            return OnDemand()
        elif self.selectedElement == 'Planner':
            return Planner()
        elif self.selectedElement == 'Messages':
            return Messages()
        elif self.selectedElement == 'Settings':
            return Settings()
        elif self.selectedElement == 'Help':
            return Help()
        else:
            return None