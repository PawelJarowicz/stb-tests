import os, sys
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../'))
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../..'))
from environment.Plato import Plato

class PINWindow(object):    
    """
        Description:
            Class to handling PIN Window.
            
        Variables:
            self.__parent: object that execute PIN Window
    """
    
    def __init__(self, parental):
        self.__parent = parent
        self.remote = Plato.remote
    def enterPIN(self, pin = '1234'):
        """
            Description:
                Method to enter PIN in PIN Window.
                
            Args:
                pin: string with PIN number.
                
            Raise:
                IndexError: raise when pin is too long.
                TypeError: raise when PIN is not a number (catch from Remote.pressButton() and raised again).
        """
        pinList = list(pin)
        if pinList.length != 4:
            raise IndexError('Wrong PIN.')
        try:
            for pinNumber in pinList:
                self.remote.pressButton(pinNumber)
            if not self.isOpen():
                return self.__parent
        except TypeError as e:
            print 'PIN should be a number.'
            raise e
        
    def cancel(self):
        """
            Description:
                Method to cancel entering PIN.
        """
        self.remote.pressButton('EXIT')
    
    def isOpen(self):
        raise NotImplementedError("Not implemented yet.")
    
class WrongPINError(RuntimeError):
    def __init__(self, message):
        self.message = message
        
    def __str__(self):
        return str(self.message)
    
class PINMenu(object):
    def __init__(self):
        raise NotImplementedError("Not implemented yet.")