import os, sys, time, random
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../'))
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../..'))
from environment.Plato import Plato
from core.ParentMenu import ParentMenu
from environment.Resources import Resources

class LiveTV(ParentMenu):

    noTVSignalCounter = 0
    directionsDictionary = {'UP':'ARROW_UP', 'DOWN':'ARROW_DOWN'}

    def __init__(self):
        super(LiveTV, self).__init__()
        self.elementsList = ['NoTVSignal']
        self.coordinatesDictionary = {'NoTVSignal': (485,290,1485,810)}
        self.templatePicturesPath = Resources().LIVE_TV_TEMPLATES_DIR
        self.remote = Plato.remote
        
    def getThis(self):
        return self

    def openMainMenu(self):
        self.remote.pressButton('MENU')

    def openOnDemand(self):
        self.remote.pressButton('ON_DEMAND')

    def openSubtitles(self):
        self.remote.pressButton('SUBTITLES')

    def openPlaylist(self):
        self.remote.pressButton('PLAYLIST')

    def TODO_openFavourites(self):
        pass

    def openLanguage(self):
        self.remote.pressButton('LANGUAGE')

    def openHelp(self):
        self.remote.pressButton('HELP')

    def openTVGuide(self):
        self.remote.pressButton('TV_GUIDE')

    def openInfoBanner(self):
        self.remote.pressButton('INFO')

    def channelPlus(self):
        self.remote.pressButton('CHANNEL_UP')

    def channelPlus(self):
        self.remote.pressButton('CHANNEL_DOWN')
        
    def isNoTVSignalShown(self):
        if (self.isSelected('NoTVSignal')):
            self.noTVSignalCounter = self.noTVSignalCounter + 1    # sorry for that
            return True
        return False
    
    def getNoTVSignalCounter(self):
        return self.noTVSignalCounter

    def openServiceList(self):
        self.remote.pressButton('EXIT')   # firstly is pressed exit to eventualy close iPlate
        self.remote.pressButton('EXIT')
        self.remote.pressButton('OK')

    def selectPreviousBouquet(self):
        self.remote.pressButton('ARROW_LEFT')

    def selectNextBouquet(self):
        self.remote.pressButton('ARROW_RIGHT')

    def navigateServiceList(self, direction):
        if (self.directionsDictionary.keys().count(direction) < 1):
            raise TypeError(direction)
        self.remote.pressButton(self.directionsDictionary[direction])

    def switchChannel(self,channelNr):
        print 'Switching channel to: '+ str(channelNr)
        for digit in channelNr:
            self.remote.pressButton(digit)
            time.sleep(1)
        self.remote.pressButton('OK')    # press OK to switch channel immediately 

    def randomiseChannelNumber(self):       # maybe it is needed refactoring of this function to return more random channelNumber
        channelNumber = ""
        numberOfDigits = random.randint(3, 4)      # get random length of string with numbers for Channel Number
        for i in range(0, numberOfDigits):
            randomInt = random.randint(0, 9)
            channelNumber = channelNumber + (str(randomInt))
        return channelNumber