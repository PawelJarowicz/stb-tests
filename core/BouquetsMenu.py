import os, sys, re, time
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../'))
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../..'))
from datetime import datetime
from core.List import List
from environment.Camera import Camera
from environment.Resources import Resources
from environment.Plato import Plato
from core.ParentMenu import ParentMenu

class BouquetsMenu(ParentMenu):

    """
        Description:
            Class provides the bouquets and the channel list.
            
        Variables:
            __bouquetsDictionary: dictionary with bouquets names and Lists of channels.
    """
    
    def __init__(self):
        super(BouquetsMenu, self).__init__()
        self.y_axis_displacement = 30 #no of pixels to add to menu with up navigation arrow. Menu without it has different coordinates
        self.current_time= datetime.now().strftime('%y-%m-%d_%H.%M.%S')
        self.templatePicturesPath = Resources().BOUQUETS_MENU_TEMPLATES_DIR
        self.bouquetsList = ['NOVA', 'Documentary', 'Entertainment', 'Kids', 'Music', 'News', 'Sport', 'HD', 'Radio','Public','DTT','Favourite_1']
        self.numbersTemplatesPath = Resources().NUMBERS_TEMPLATES_DIR

        self.numberPosition= [
                              [(147,236,163,260),(165,236,181,260),(183,236,199,260),(201,236,217,260)],
                              [(147,308,163,332),(165,308,181,332),(183,308,199,332),(201,308,217,332)],
                              [(147,380,163,404),(165,380,181,404),(183,380,199,404),(201,380,217,404)],
                              [(147,452,163,476),(165,452,181,476),(183,452,199,476),(201,452,217,476)],
                              [(147,524,163,548),(165,524,181,548),(183,524,199,548),(201,524,217,548)],
                              [(147,596,163,620),(165,596,181,620),(183,596,199,620),(201,596,217,620)],
                              [(147,668,163,692),(165,668,181,692),(183,668,199,692),(201,668,217,692)],
                              [(147,740,163,764),(165,740,181,764),(183,740,199,764),(201,740,217,764)],
                              [(147,812,163,836),(165,812,181,836),(183,812,199,836),(201,812,217,836)],
                              [(147,884,163,908),(165,884,181,908),(183,884,199,908),(201,884,217,908)]
                            ]

        self.dotPosition= [
                           [(201,251,210,260),(219,251,228,260)],
                           [(201,323,210,332),(219,323,228,332)],
                           [(201,395,210,404),(219,395,228,404)],
                           [(201,467,210,476),(219,467,228,476)],
                           [(201,539,210,548),(219,539,228,548)],
                           [(201,611,210,620),(219,611,228,620)],
                           [(201,683,210,692),(219,683,228,692)],
                           [(201,755,210,764),(219,755,228,764)],
                           [(201,827,210,836),(219,827,228,836)],
                           [(201,899,210,908),(219,899,228,908)]
                          ]

        self.coordinatesDictionary = {'NOVA'         :(387,153,430,176),
                                      'Documentary'  :(387,153,430,176),
                                      'Entertainment':(387,153,430,176),
                                      'Kids'         :(387,153,430,176),
                                      'Music'        :(387,153,430,176),
                                      'News'         :(387,153,430,176),
                                      'Sport'        :(387,153,430,176),
                                      'HD'           :(387,153,430,176),
                                      'Radio'        :(387,153,430,176),
                                      'Public'       :(387,153,430,176),
                                      'DTT'          :(387,153,430,176),
                                      'Favourite_1'  :(387,153,430,176),
                                      'ARROW_LEFT'   :(163,150,183,180),
                                      'ARROW_RIGHT'  :(633,150,653,180),
                                      'ARROW_UP'     :(393,210,423,230),
                                      'ARROW_DOWN'   :(393,975,423,995)
                                      }
    def __add_displacemet_to_coordinates(self,coordinates):
        return coordinates[0], coordinates[1] + self.y_axis_displacement, coordinates[2], coordinates[3] + self.y_axis_displacement

    def isUpNavigationArrowVisible(self):
        tresholdedScreenshot, croppedTresholdedScreenshot = Camera.quickScreenshot(self.coordinatesDictionary['ARROW_UP'])
        Camera.saveImage(croppedTresholdedScreenshot, Resources.SCREENSHOTS_DIR+'ARROW_UP_cropped.png')
        if Plato.checkIfImagesSimilar(croppedTresholdedScreenshot,self.templatePicturesPath+'ARROW_UP.png',self.ACCEPTANCE_LIMIT):
            return True
        return False
    
    def getSelectedRow(self):
        """
        Checks which row is selected in Bouquets menu, by checking if dot is bold.
        Depending if channel number has 3 or 4 digits, position of dot changes.
        1) Taking screenshot
        2) Threshold screenshot
        3) Crop screenshot to selected dot position
        4) Comparing crop to template of bold dot
        5) If found: return row number, Else continue searching
        6) If method cannot find selected row, then it will raise an exception
        
        Return -1 if cannot find row.
        """
        recalculatePosition = None
        #1
        fullScreenshot= Camera.captureScreenshot()
        #2
        fullScreenshot_TH = Camera.filterImage(fullScreenshot, 'BW')
        Camera().saveImage(fullScreenshot_TH, Resources().SCREENSHOTS_FULL_DIR + self.current_time + '_full.png')
        
        if self.isUpNavigationArrowVisible():
            recalculatePosition = True
        else:
            recalculatePosition = False
        
        for row in range(len(self.dotPosition)):
            for column in range(len(self.dotPosition[row])):
                if recalculatePosition:
                    coordinates = self.__add_displacemet_to_coordinates(self.dotPosition[row][column])
                else:
                    coordinates = self.dotPosition[row][column]
                #3
                dotCropp = Camera.cropImage(fullScreenshot_TH, coordinates)
                Camera().saveImage(dotCropp, Resources().SCREENSHOTS_DIR +'dot['+str(row)+']['+str(column)+']')
                Plato.checkImagesSimilarity(dotCropp,self.templatePicturesPath+'dotb.png')
                #4
                if Plato.checkIfImagesSimilar(dotCropp,self.templatePicturesPath+'dotb.png',self.ACCEPTANCE_LIMIT):
                    #5
                    return row+1
        return -1

    def open(self):
        self.remote.pressButton('EXIT')    # press EXIT for close other menus (like LiveBanner)
        self.remote.pressButton('OK')

    def close(self):
        self.remote.pressButton('EXIT')

    def next(self):
        self.remote.pressButton('ARROW_RIGHT')

    def previous(self):
        self.remote.pressButton('ARROW_LEFT')

    def isOpened(self):
        return self.isSelected('ARROW_LEFT')

    def up(self):
        self.remote.pressButton('ARROW_UP')

    def down(self):
        self.remote.pressButton('ARROW_DOWN')

    def confirmSelection(self):
        self.open()

    def getChannelNumber(self, row):
        """
        Returns channel number from bouquets menu:
        for specified row, function gets screen of number for first 4 numbers.
        If screen matches one of templates, number is added to list.
        After search is done, list is returned.
        Return empty string if row number is smaller than 0.
        """

        recalculatePosition = None
        ACCEPTANCE_LIMIT = 0.2
        channel = ""
        
        row = int(row)-1
        
        if row < 0:
            return channel
        
        fullScreenshot= Camera.captureScreenshot()
        fullScreenshot_TH = Camera.filterImage(fullScreenshot, 'BW')
        Camera().saveImage(fullScreenshot_TH, Resources().SCREENSHOTS_FULL_DIR + self.current_time + '_full.png')
        
        if self.isUpNavigationArrowVisible():
            recalculatePosition = True
        else:
            recalculatePosition = False
        
        for column in range(len(self.numberPosition[row])):
            if recalculatePosition:
                coordinates = self.__add_displacemet_to_coordinates(self.numberPosition[row][column])
            else:
                coordinates = self.numberPosition[row][column]
            
            nrCropp = Camera.cropImage(fullScreenshot_TH, coordinates)
            Camera().saveImage(nrCropp, Resources().SCREENSHOTS_DIR +'number['+str(row)+']['+str(column)+']')
            for numberTemplate in os.listdir(self.numbersTemplatesPath):
                Plato.checkImagesSimilarity(nrCropp,self.numbersTemplatesPath+numberTemplate)
                if Plato.checkIfImagesSimilar(nrCropp,self.numbersTemplatesPath+numberTemplate,ACCEPTANCE_LIMIT):
                    channel=channel+str(re.search('(\d)', numberTemplate).group(1))
        return channel

    def getOpenedBouquetName(self):
        """
            Description:
                  it takes middle part of name and compare to same part from template.
                  I've left full names of templates mainly because one can recognize what it relays to. 
        """
        nameFragmentCoordinates = self.coordinatesDictionary.get('NOVA')

        tresholdedScreenshot, croppedTresholdedScreenshot = Camera.quickScreenshot(nameFragmentCoordinates)
        Camera().saveImage(croppedTresholdedScreenshot, Resources.SCREENSHOTS_DIR+'bouquetName')
        for bouquetName in self.bouquetsList:
            if Plato.checkIfImagesSimilar(croppedTresholdedScreenshot,self.templatePicturesPath+bouquetName+'.png',self.ACCEPTANCE_LIMIT):
                return bouquetName
        return ""

    class SelectedRowNotFoundException(Exception):
        def __init__(self):
            Exception.__init__(self,"Cannot find selected bouquet row!") 
            

