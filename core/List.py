import os, sys
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../'))
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../..'))
from environment.Plato import Plato

class List(object):
    def __init__(self, elementsList = []):
        self.elementsList = elementsList
        self.remote = Plato.remote
        self.selectedElement = None
    
    def selectElement(self, element):
        if not self.elementsList:       # check if list is not empty
            raise IndexError()
        
        selectedElementIndex = self.elementsList.index(self.selectedElement)
        indexToSelect = self.elementsList.index(element)
        
        if selectedElementIndex != indexToSelect: # if same, do nothing
            diff = indexToSelect - selectedElementIndex
            while diff != 0:
                if diff > 0:
                    self.__down()
                    diff = diff - 1
                if diff < 0:
                    self.__up()
                    diff = diff + 1
            self.selectedElement = element
                    
    def getSelectedElement(self):
        return self.selectedElement
                    
    def __up(self):
        self.remote.pressButton('ARROW_UP')
        selectedElementIndex = self.elementsList.index(self.selectedElement)
        self.selectedElement = self.elementsList[selectedElementIndex - 1]
        
    def __down(self):
        self.remote.pressButton('ARROW_DOWN')
        selectedElementIndex = self.elementsList.index(self.selectedElement)
        self.selectedElement = self.elementsList[selectedElementIndex + 1]
        
    def setSelectedElement(self, element):
        self.selectedElement = element
        
    def first(self):
        return self.elementsList[0]

    def add(self, element):
        self.elementsList.append(element)
        
    def isEmpty(self):
        if len(self.elementsList) == 0:
            return True
        else:
            return False
        
    def getElementsList(self):
        return self.elementsList