import os, sys, abc
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../'))
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../..'))
from ParentMenu import ParentMenu
from environment.Plato import Plato

class Menu(ParentMenu):
    __metaclass__ = abc.ABCMeta
    
    def __init__(self):
        super(Menu, self).__init__()
    
    def openSelected(self):
        pass
                   
    def getSelectedElement(self):
        return self.selectedElement