import os, sys
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../'))
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../..'))
from environment.Remote import Remote
from environment.Plato import Plato

class Keyboard(object):
    """
        Description:
            Class to handle keyboard in Plato STBs.
            
        Variables:
            __CHARACTERS: dictionary with letters coordinates.
            __SYMBOLS: dictionary with symbols coordinates.
            __row: selected row.
            __column: selected column.
            __ROWS_NUMBER: number of all rows.
            __COULMNS_NUMBER: number of all columns.
            __selectedKeyboard: selected keyboard ('LETTERS' or 'SYMBOLS')
    """
    __CHARACTERS = {"A":(1,1), "B":(1,2), "C":(1,3), "D":(1,4), "E":(1,5), "F":(1,6), "G":(2,1), "H":(2,2), "I":(2,3), "J":(2,4), "K":(2,5), "L":(2,6),"M":(3,1), "N":(3,2), "O":(3,3), "P":(3,4), "Q":(3,5), "R":(3,6), "S":(4,1), "T":(4,2), "U":(4,3), "V":(4,4), "W":(4,5), "X":(4,6), "Y":(5,1), "Z":(5,2), "1":(5,3), "2":(5,4), "3":(5,5), "4":(5,6), "5":(6,1), "6":(6,2), "7":(6,3), "8":(6,4), "9":(6,5), "0":(6,6)}
    __SYMBOLS = {"!":(1,1), "?":(1,2), "@":(1,3), "#":(1,4), "$":(1,5), "%":(1,6), "&":(2,1), "*":(2,2), "(":(2,3), ")":(2,4), "-":(2,5), "_":(2,6), "=":(3,1), "+":(3,2), "[":(3,3), "]":(3,4), "^":(3,5), "/":(3,6), "\\":(4,1), "|":(4,2), ";":(4,3), ":":(4,4), ",":(4,5), ".":(4,6), "<":(5,1), ">":(5,2), "'":(5,3), "\"":(5,4)}
    __row = 1
    __column = 1
    __ROWS_NUMBER = 6
    __COULMNS_NUMBER = 6
    
    def __init__(self):
        """
            Description:
                Constructor.
        """
        self.__selectedKeyboard = 'LETTERS'
        self.remote = Plato.remote
    
    def __rowUp(self):
        """
            Description:
                Method that switch to one row up. If focus is on first row then no switch up.
        """
        if self.__row >= 0:
            self.remote.pressButton('ARROW_UP')
            self.__row = self.__row - 1
        
    def __rowDown(self):
        """
            Description:
                Method that switch to one row down. If focus is on last row then no switch down.
        """
        if self.__row < self.__ROWS_NUMBER:
            self.remote.pressButton('ARROW_DOWN')
            self.__row = self.__row + 1
            
    def __columnRight(self):
        """
            Description:
                Method that switch to one column right. If focus is on first column then no switch right.
        """
        if self.__column  < self.__COULMNS_NUMBER:
            self.remote.pressButton('ARROW_RIGHT')
            self.__column = self.__column + 1
        
    def __columnLeft(self):
        """
            Description:
                Method that switch to one column left. If focus is on last column then no switch left.
        """
        if self.__column > 1:
            Remote().pressButton('ARROW_LEFT')
            self.__column = self.__column - 1
            
    def __setFocusOnFirstLetter(self):
        """
            Description:
                Method that set focus on first letter on keyboard.
        """
        while self.__column > 1:
            self.__columnLeft()
        while self.__row > 1:
            self.__rowUp()
            
    def __setColumn(self, column):
        """
            Description:
                Method that set focus on selected column.
                
            Args:
                column: number of column to set.
        """
        if column > self.__column:
            while self.__column != column:
                self.__columnRight()
        elif column < self.__column:
            while self.__column != column:
                self.__columnLeft()
    
    def __setRow(self, row):
        """
            Description:
                Method that set focus on selected row.
                
            Args:
                row: number of row to set.
        """
        if row > self.__row:
            while self.__row != row:
                self.__rowDown()
        elif row < self.__row:
            while self.__row != row:
                self.__rowUp()
                
    def __enterCharacter(self, letter = 'A'):
        """
            Description:
                Method that enter character to search box.
                
            Args:
                letter: string with character to enter to search box.
        """
        if letter == " ":
            self.__setColumn(2)
            self.__setRow(0)
        else:
            (r,c) = self.__CHARACTERS[letter]
            self.__setRow(r)
            self.__setColumn(c)
        self.remote.pressButton('OK')
    
    def enterText(self, text):
        """
            Description:
                Method that enter text on keyboard.
                
            Args:
                text: text to enter.
        """
        textList = list(text.upper())
        for charToEnter in textList:
            if self.__CHARACTERS.keys().count(charToEnter) > 0:
                self.__selectKeyboard('LETTERS')
                self.__enterCharacter(letter = charToEnter)
            elif self.__SYMBOLS.keys().count(charToEnter) > 0:
                self.__selectKeyboard('SYMBOLS')
                self.__enterSymbol(charToEnter)
            
    def __enterSymbol(self, symbol = "?"):
        """
            Description:
                Method that enter symbol in searchbox.
                
            Args:
                symbol: string with symbol to enter.
        """
        (r,c) = self.__SYMBOLS[symbol]
        if r == 5:
            self.__setColumn(4)
        self.__setRow(r)
        self.__setColumn(c)
        self.remote.pressButton('OK')
    
    def __selectKeyboard(self, keyboard = 'LETTERS'):
        """
            Description:
                Method that select symbols or letters keyboard.
                
            Args:
                keyboard: 'LETTERS' or 'SYMBOLS'
        """
        if self.__selectedKeyboard != keyboard:
            self.__setColumn(4)
            self.__setRow(0)
            self.remote.pressButton('OK')
            self.__rowDown()
            self.__selectedKeyboard = keyboard
 
    def removeCharacters(self, howManyChars = 1):
        """
            Description:
                Method that remove one (last) character from entered in search box.
                
            Args:
                howManyChars: number of characters to remove.
        """
        self.__setColumn(6)
        self.__setRow(0)
        for i in range(0, howManyChars):
            self.remote.pressButton('OK')
        self.__rowDown()
        
    def getColumn(self):
        """
            Description:
                Returns selected column.
        """
        return self.__column
    
    def getRow(self):
        """
            Description:
                Returns selected row.
        """
        return self.__row
    
    def setColumn(self, column):
        """
            Description:
                Sets __column value.
        """
        self.__column = column
        
    def setRow(self, row):
        """
            Description:
                Sets __row value.
        """
        self.__row = row
        
    def setFocusOnFirstLetter(self):
        """
            Description:
                Method to set focus on first letter in keyboard.
        """
        while self.__column > 1:
            self.__columnLeft()
        while self.__row > 1:
            self.__rowUp()