import os, sys

sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../'))
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../..'))
from environment.Plato import Plato
from environment.Camera import Camera
from environment.Resources import Resources


class InfoBanner(object):
    def __init__(self):
        self.ACCEPTANCE_LIMIT = 0.07
        self.remote = Plato.remote
        self.coordinatesDictionary = {'ARROW_LEFT':(62,897,81,924)}
        self.templatePicturesPath = Resources().INFO_BANNER_TEMPLATES_DIR
        
    def open(self):
        for i in range(0, 4):
            self.remote.pressButton('EXIT')
        self.remote.pressButton('INFO')

    def isOpened(self):
        """
            Checks if info banner is opened by checking visibility of left arrow visible on it
        """
        tresholdedScreenshot, croppedTresholdedScreenshot = Camera.quickScreenshot(self.coordinatesDictionary['ARROW_LEFT'])
        Camera.saveImage(croppedTresholdedScreenshot, Resources.SCREENSHOTS_DIR + 'INFOBANNER_ARROW_LEFT_cropped.png')
        return Plato.checkIfImagesSimilar(croppedTresholdedScreenshot,self.templatePicturesPath+'ARROW_LEFT.png', self.ACCEPTANCE_LIMIT)