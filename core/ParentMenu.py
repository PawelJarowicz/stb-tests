import os, sys, abc, cv2, time
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../'))
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../..'))
from List import List
from environment.Plato import Plato
from environment.Camera import Camera
from environment.Resources import Resources
from datetime import datetime

class ParentMenu(List):
    __metaclass__ = abc.ABCMeta
    
    def __init__(self):
        super(ParentMenu, self).__init__()
        self.coordinatesDictionary = {}
        self.templatePicturesPath = ""
        self.ACCEPTANCE_LIMIT = 0.07
    
    def openElement(self, element):
        self.selectElement(element)
        openedElement = self.openSelected()
        time.sleep(1) 
        return openedElement
    
    def exitToLiveTV(self):
        for i in range(0, 4):       # pressing EXIT button four times is a secure way to get back to LiveTV, even if one signal from remote to box is lost
            self.remote.pressButton('EXIT')

    def isSelected(self, element):
        destinationPath      = Resources().SCREENSHOTS_DIR + element

        current_time         = datetime.now().strftime('%y-%m-%d_%H.%M.%S')
        tresholdedScreenshot, croppedTresholdedScreenshot= Camera().quickScreenshot(self.coordinatesDictionary[element])
        Camera().saveImage(tresholdedScreenshot, Resources().SCREENSHOTS_FULL_DIR + current_time + '_full.png')
        Camera().saveImage(croppedTresholdedScreenshot, destinationPath+'_cropped.png')
        print self.templatePicturesPath+ str(element) + '.png'
        return Plato.checkIfImagesSimilar(croppedTresholdedScreenshot, self.templatePicturesPath+ str(element) + '.png', self.ACCEPTANCE_LIMIT)
    
    def isOpened(self):
        return self.isSelected(self.selectedElement)
    
    def getCoordinatesDictionary(self):
        return self.coordinatesDictionary