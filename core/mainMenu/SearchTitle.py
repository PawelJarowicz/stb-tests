import os, sys
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../'))
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../..'))
from core.Keyboard import Keyboard
from environment.Plato import Plato

class SearchTitle(object):

    __ORDERS_OPTIONS = ["AZ", "Date"]
    __SOURCES_OPTIONS = ["ALL", "TV", "Playlist"]
    __SEARCH_IN_OPTIONS = ["Title", "Cast"]

    def __init__(self, params):
        self.keyboard = Keyboard()
        self.__order = "AZ"
        self.__source = "ALL"
        self.__searchIn = "Title"
        self.remote = Plato.remote
        
    def __setSearchParams(self, optionsList, selectedOption, toSelectOption):
        selectedIndex = optionsList.index(selectedOption)
        toSelectIndex = optionsList.index(toSelectOption)
        
        if selectedindex > toSelectIndex:
            for i in range(toSelectIndex, selectedindex):
                self.remote.pressButton('ARROW_DOWN')
        elif selectedindex < toSelectIndex:
            for i in range(selectedindex, toSelectIndex):
                self.remote.pressButton('ARROW_UP')
        
        selectedOption = toSelectOption
        self.remote.pressButton('OK')
        
    def TODO_search(self, text):
        self.keyboard.enterText(text)
        
    def setOrder(self, order):
        if self.__ORDERS_OPTIONS.count(source) == 0:
            raise ValueError('Selected source option does not exist.')
        # navigate to Order
        self.keyboard.setFocusOnFirstLetter()
        for i in range(0, 3):
            self.remote.pressButton('ARROW_UP')
        self.remote.pressButton('ARROW_LEFT')
        self.remote.pressButton('OK')
        # set Order
        self.__setSearchParams(self.__ORDERS_OPTIONS, self.__order, order)
        # navigate to first letter on keyboard
        for i in range(0, 3):
            self.remote.pressButton('ARROW_DOWN')
        self.keyboard.setColumn(1)
        self.keyboard.setRow(1)
    
    def setSource(self, source):
        if self.__SOURCES_OPTIONS.count(source) == 0:
            raise ValueError('Selected source option does not exist.')
        # navigate to Source
        self.keyboard.setFocusOnFirstLetter()
        for i in range(0, 3):
            self.remote.pressButton('ARROW_UP')
        self.remote.pressButton('OK')
        # set Source
        self.__setSearchParams(self.__SOURCES_OPTIONS, self.__source, source)
        # navigate to first letter on keyboard
        for i in range(0, 3):
            self.remote.pressButton('ARROW_DOWN')
        self.keyboard.setColumn(1)
        self.keyboard.setRow(1)
    
    def setSearchIn(self, searchIn):
        if self.__SEARCH_IN_OPTIONS.count(searchIn) == 0:
            raise ValueError('Selected searchIn option does not exist.')
        # navigate to SearchIn
        self.keyboard.setFocusOnFirstLetter()
        for i in range(0, 2):
            self.remote.pressButton('ARROW_UP')
        self.remote.pressButton('OK')
        # set SearchIn
        self.__setSearchParams(self.__SEARCH_IN_OPTIONS, self.__searchIn, searchIn)
        # navigate to first letter on keyboard
        for i in range(0, 2):
            self.remote.pressButton('ARROW_DOWN')
        self.keyboard.setColumn(1)
        self.keyboard.setRow(1)
        