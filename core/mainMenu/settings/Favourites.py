import os, sys
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../'))
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../..'))
from environment.Remote import Remote
from core.Keyboard import Keyboard
from core.mainMenu.settings.ChannelsSelection import ChannelsSelection
from core.List import List

class Favourites(List):
    """
        Description:
            Class inherits from List. Class to handling Favorites list.
            
        Variables:
            self.favouritesLists: dictionary with favorites list names and list of added to favourites list channels.
            self.elementsList: inherited from List. List with names of favorites lists.
            self.selectedElement: inherited from List. Selected favorites list.
    """
    def __init__(self):
        """
            Description:
                Class constructor.
        """
        super(List, self).__init__()
        self.favouritesLists = {'Favourite1': List(), 'Favourite2': List(), 'Favourite3': List(), 'Favourite4': List(), 'Favourite5': List()}
        self.elementsList = self.favouritesLists.keys()
        self.setSelectedElement(self.elementsList[0])
        
        
    def saveSettings(self):
        """
            Description:
                Method that save settings (by EXIT button). Return Settings object with focus set on Favourites.
        """
        Remote().pressButton('EXIT')
    
    def renameSelectedList(self, newName):
        """
            Description:
                Method that rename selected favorites list.
                
            Args:
                newName: new name of selected list.
        """
        Remote().pressButton('ARROW_RIGHT')
        Remote().pressButton('OK')
        keyboard = Keyboard()
        keyboard.removeCharacters(20)
        keyboard.enterText(newName)
        for i in range(0, keyboard.getColumn() + 1):
            Remote().pressButton('ARROW_RIGHT')
        Remote().pressButton('OK')
    
    def editFavouritesList(self, favouritesList):
        """
            Description:
                Method that opens select favorites list to edit. Returns ChannelsSelection object to edit selected favourites list.
                
            Args:
                favouritesList: list to edit.
        """
        self.selectElement(favouritesList)
        Remote().pressButton('OK')
        return ChannelsSelection(self)      # ChannelsSelection gets in constructor Favourites object.
    
    def renameList(self, listToRename, newName):
        """
            Description:
                Method that selects favorites list and rename it.
                
            Args:
                listToRename: favorites list to rename.
                newName: new name of list.
        """
        self.selectElement(listToRename)
        self.renameSelectedList(newName)