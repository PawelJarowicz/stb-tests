import os, sys
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../'))
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../..'))
from core.mainMenu.settings.SubMenu import SubMenu
from environment.Remote import Remote

class Language(SubMenu):

    def __init__(self):
        super(SubMenu, self).__init__()
        self.elementsList = ['DefaultMenuLanguage']
        self.setSelectedElement(self.elementsList[0])