import abc, os, sys
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../'))
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../..'))
from core.ParentMenu import ParentMenu
from environment.Plato import Plato

class SubMenu(ParentMenu):
    __metaclass__ = abc.ABCMeta
    
    def __init__(self):
        super(SubMenu, self).__init__()
                    
    def changeSetting(self, element):
        self.selectElement(element)
        self.remote.pressButton('ARROW_RIGHT')
        
    def saveSettings(self):
        self.remote.pressButton('OK')
        
    def getSelectedElement(self):
        return self.selectedElement