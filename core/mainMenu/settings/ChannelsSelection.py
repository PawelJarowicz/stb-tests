import os, sys
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../'))
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../..'))
from core.List import List
from environment.Remote import Remote

class ChannelsSelection(object):
    """
        Description:
            Class for handling Channels Selection window.
            
        Variables:
            self.parrent: Favourites object which call ChannelsSelection object.
            self.favouritesList: name of edited favourites list.
            self.bouquetsList: List object with names of bouquets.
            self.availableChannelsList: list of available channels (channels list from selected bouquet).
            self.addedChannelsList: List object with channels added to selected favourites list.
            self.tabs: list of three tabs which can be selected in channels selection window.
            self.__selectedTab: selected Tab name.
    """
    
    def __init__(self, parent):
        """
            Description:
                Class constructor.
                
            Args:
                parent: Favourites object which call ChannelsSelection object.
        """
        self.parent = parent
        self.favouritesList = self.parent.getSelectedElement()
        self.bouquetsList = List(Bouquets().getBouquetsList())
        self.availableChannelsList = Bouquets().getChannelsList(self.bouquetsList[0])
        self.addedChannelsList = self.parent.favouritesLists(self.favouritesList)
        self.tabs = ['Bouquets', 'ChannelsToAdd', 'ChannelsAdded']
        self.__selectedTab = self.tabs[0]
        
    def __selectTab(self, tab):
        """
            Description:
                Method that select tab.
                
            Args:
                tab: tab to select.
        """
        selectedIndex = self.tabs.index(self.__selectedTab)
        toSelectIndex = self.tabs.index(tab)
            
        if selectedIndex != toSelectIndex:   # if same, do nothing
            if selectedIndex > toSelectIndex:
                while toSelectIndex != selectedIndex:
                    Remote().pressButton('ARROW_RIGHT')
                    selectedIndex = self.tabs.index(self.__selectedTab)
                        
            else:
                while selectedIndex != toSelectIndex:
                    Remote().pressButton('ARROW_LEFT')
                    selectedIndex = self.tabs.index(self.__selectedTab)
                        
            self.__selectedTab = tab
    
    def selectBouquet(self, bouquet):
        """
            Description:
                Method that select bouquet from bouquets list.
                
            Args:
                bouquet: bouquet to select.
        """
        if self.__selectedTab != 'Bouquets':     # if same, do nothing
            self.__selectTab('Bouquets')
            self.bouquetsList.setSelectedElement(self.bouquetsList.first())     # after select tab focus goes to first element on list.
        self.availableChannelsList = Bouquets().getChannelsList(bouquet)    # set proper channels List
        self.bouquetsList.selectElement(bouquet)
        
    def selectChannelToAdd(self, channel):
        """
            Description:
                Method that select channel on channels list.
                
            Args:
                channel: channel to select.
        """
        if self.__selectedTab != 'ChannelsToAdd':     # if same, do nothing
            self.__selectTab('ChannelsToAdd')
            try:
                self.availableChannelsList.setSelectedElement(self.availableChannelsList.first())     # after select tab focus goes to first element on list.
            except IndexError:
                raise Warning('Channels list is empty.')     # if first method raise IndexError then raise Warning with information about empty channels list.
        try:
            self.availableChannelsList.selectElement(channel)
        except IndexError:
            raise Warning('Selected channel is not available on this bouquet.')     # if selectElement method raise IndexError then raise Warning with proper information.
        
    def addChannelToFavourites(self, channel):
        """
            Description:
                Method that add channel to favourites list.
                
            Args:
                channel: channel to add.
        """
        self.selectChannelToAdd(channel)
        self.addedChannelsList.add(channel)
        Remote().pressButton('OK')
        
    def selectChannelAlreadyAdded(self, channel):
        """
            Description:
                Method that select channel from already added list.
                
            Args:
                channel: channel to select.
        """
        if self.addedChannelsList.isEmpty():
            print 'There is no channels added to favourites.'
        else:
            if self.__selectedTab != 'ChannelsAdded':
                self.__selectedTab('ChannelsAdded')
                self.addedChannelsList.setSelectedElement(self.addedChannelsList.first())
            self.addedChannelsList.selectElement(channel)
            
    def saveChannels(self):
        """
            Description:
                Method that save channels and return Favourites list which call this object.
        """
        Remote().pressButton('EXIT')
        self.parent.favouritesLists[self.favouritesList] = self.addedChannelsList
        return self.parent