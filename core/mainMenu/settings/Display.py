import os, sys
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../'))
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../..'))
from core.mainMenu.settings.SubMenu import SubMenu
from environment.Remote import Remote

class Display(SubMenu):

    def __init__(self):
        super(SubMenu, self).__init__()
        self.elementsList = ['DefaultSubtitleOption', 'DefaultSubtitleLanguage', 'InfoBannerTimeout', 'ReminderAdvanceTime']
        self.setSelectedElement(self.elementsList[0])