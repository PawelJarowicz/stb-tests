import os, sys
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../'))
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../..'))
from core.mainMenu.settings.SubMenu import SubMenu
from environment.Plato import Plato
from environment.Camera import Camera
from environment.Resources import Resources

class Scanning(SubMenu):

    def __init__(self):
        super(Scanning, self).__init__()
        self.elementsList = ['SAT', 'SATandDTT', 'DiSEqC']
        self.setSelectedElement(self.elementsList[0])
        self.coordinatesDictionary = {'SAT': (975,431,1416,492), 'SATandDTT': (975,509,1416,570), 'DiSEqC': (975,587,1416,648)}
        self.templatePicturesPath = Resources().ADVANCED_SETTINGS_TEMPLATES_DIR
    
    def exit(self):
        self.remote.pressButton('EXIT')
        
    def scan(self):
        self.remote.pressButton('OK')
        
    def isScanComplete(self):
        ACCEPTANCE_LIMIT = 0,02
        screenName = 'scanComplete'
        scanCompleteMsgCoordinates = (615,320,843,345)
        cropSaveDir = Resources().SCREENSHOTS_DIR+screenName+'_cropped.png'
        tresholdedScreenshot, croppedTresholdedScreenshot= Camera().quickScreenshot(scanCompleteMsgCoordinates)
        Camera().saveImage(croppedTresholdedScreenshot, cropSaveDir)
        return Plato.checkIfImagesSimilar(croppedTresholdedScreenshot, Resources().ADVANCED_SETTINGS_TEMPLATES_DIR + screenName+'.png', self.ACCEPTANCE_LIMIT)

    def openSelected(self):    
        if self.selectedElement == 'SAT':
            self.scan()
        elif self.selectedElement == 'SATandDTT':
            self.scan()
        elif self.selectedElement == 'DiSEqC':
            raise NotImplementedError("Not implemented yet.")
        else:
            return None