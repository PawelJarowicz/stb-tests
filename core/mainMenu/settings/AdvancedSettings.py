import os, sys
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../'))
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../..'))
from core.Menu import Menu
from environment.Plato import Plato
from environment.Resources import Resources
from core.mainMenu.settings.advancedSettings.Reset import Reset
from core.mainMenu.settings.advancedSettings.Scanning import Scanning
from core.mainMenu.settings.advancedSettings.FactoryReset import FactoryReset
from core.mainMenu.settings.advancedSettings.SoftwareUpgrade import SoftwareUpgrade

class AdvancedSettings(Menu):

    def __init__(self):
        super(AdvancedSettings, self).__init__()
        self.elementsList = ['Reset', 'Scanning', 'FactoryReset', 'SoftwareUpgrade']
        self.setSelectedElement(self.elementsList[0])
        self.coordinatesDictionary = {'Reset': (120,225,345,280),
                                      'Scanning': (120,285,345,340),
                                      'FactoryReset': (120,360,345,415),
                                      'SoftwareUpgrade': (120,435,345,490)
                                      }
        self.templatePicturesPath = Resources().ADVANCED_SETTINGS_TEMPLATES_DIR
        
    def exit(self):
        self.remote.pressButton('EXIT')
    
    def openSelected(self):
        self.remote.pressButton('OK')
        
        if self.selectedElement == 'Reset':
            return Reset()
        elif self.selectedElement == 'Scanning':
            return Scanning()
        elif self.selectedElement == 'FactoryReset':
            return FactoryReset()
        elif self.selectedElement == 'SoftwareUpgrade':
            return SoftwareUpgrade()
        else:
            return None