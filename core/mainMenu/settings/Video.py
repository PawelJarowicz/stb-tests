import os, sys
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../'))
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../..'))
from core.mainMenu.settings.SubMenu import SubMenu
from environment.Remote import Remote

class Video(SubMenu):

    def __init__(self):
        super(Video, self).__init__()
        self.elementsList = ['HDTVMode', 'TVAspectRatio', 'VideoOutputFormat', 'Scart', 'RFModulation', 'UHFChannel']
        self.setSelectedElement(self.elementsList[0])