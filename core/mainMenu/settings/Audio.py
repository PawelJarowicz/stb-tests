import os, sys
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../'))
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../..'))
from core.mainMenu.settings.SubMenu import SubMenu
from environment.Remote import Remote

class Audio(SubMenu):

    def __init__(self):
        super(Audio, self).__init__()
        self.elementsList = ['DefaultAudioLanguage', 'HDMIAudioFromat', 'SPDIFAudioFormat', 'SPDIFAudioDelay']
        self.setSelectedElement(self.elementsList[0])