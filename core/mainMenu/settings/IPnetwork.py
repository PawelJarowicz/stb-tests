import os, sys
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../'))
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../..'))
from core.mainMenu.settings.SubMenu import SubMenu
from environment.Remote import Remote
import re

class IPNetwork(SubMenu):

    def __init__(self):
        super(SubMenu, self).__init__()
        self.elementsList = ['Configuration', 'IPAddress', 'Gateway', 'Netmask', 'DNS']
        self.setSelectedElement(self.elementsList[0])
    
    def TODO_isSelectedManual(self):
        raise NotImplementedError("Not implemented yet.")
    
    def __removeText(self):
        for i in range(0, 12):      # four elements with max three characters = 12
            Remote().pressButton('ARROW_RIGHT')
    
    def __enterText(self, text):
        textList = list(text)
        for character in textList:
            Remote().pressButton(character)
        if len(textList) < 3:       # if entered part of ip is smaller than 100 (have 2 or less characters length) then focus does not go to next part.
            Remote().pressButton('ARROW_RIGHT')
            
    def __enterManualSettings(self, elementToEnter, text):
        textList = re.split('.', text)
        if self.TODO_isSelectedManual():
            self.selectElement(elementToEnter)
            self.__removeText()
            if len(textList) != 4:
                raise ValueError(elementToEnter + ' should have exactly 4 elements.')
            else:
                lengthOfLastPart = 0
                for i in range(0, len(textList)):
                    self.__enterText(text[i])
                    lengthOfLastPart = text[i].length
                if lengthOfLastPart == 3:       # if length of last part is 3 characters then focus goes to next section (e.g. IP Address -> Gateway)
                    selectedIndex = self.elementsList.index(self.selectedElement)
                    self.selectedElement = self.elementsList[selectedIndex + 1]
                
    def TODO_enterIPAddress(self, ipAddress):
        self.__enterManualSettings('IPAddress', ipAddress)
    
    def TODO_enterGateway(self, gateway):
        self.__enterManualSettings('Gateway', gateway)
    
    def TODO_enterNetmask(self, netmask):
        self.__enterManualSettings('Netmask', netmask)
    
    def TODO_enterDNS(self, dns):
        self.__enterManualSettings('DNS', dns)
        self.selectedElement = 'DNS'    # focus allways stay in DNS section even if length of last part of DNS is 3 characters.