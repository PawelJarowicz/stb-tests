import os, sys
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../'))
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../..'))
from environment.Plato import Plato
from Menu import Menu
from BlockByAgeRating import BlockByAgeRating
from BlockChannels import BlockChannels
from core.PINWindow import PINWindow
from ChangePINCode import ChangePINCode
from core.PINWindow import WrongPINError
from core.PINWindow import PINMenu

class ParentalControl(PINMenu):

    # TO-DO: Add other menu methods.

    def __init__(self):
        super(PINMenu, self).__init__()
        self.elementsList = ['BlockByAgeRating', 'BlockChannels', 'ChangePINCode']
        self.setSelectedElement(self.elementsList[0])
        
    def openSelected(self, pin = '1234'):
        self.remote.pressButton('OK')
        
        if self.selectedElement == 'BlockByAgeRating':
            pinWindow = PINWindow()
            pinWindow.enterPIN(pin)
            if not pinWindow.isOpen():
                return BlockByAgeRating()
            else:
                raise WrongPINError('Entered PIN is wrong.')
        elif self.selectedElement == 'BlockChannels':
            pinWindow = PINWindow()
            pinWindow.enterPIN(pin)
            if not pinWindow.isOpen():
                return BlockChannels()
            else:
                raise WrongPINError('Entered PIN is wrong.')
        elif self.selectedElement == 'ChangePINCode':
            pinWindow = PINWindow()
            pinWindow.enterPIN(pin)
            if not pinWindow.isOpen():
                return ChangePINCode()
            else:
                raise WrongPINError('Wrong PIN.')
        else:
            return None