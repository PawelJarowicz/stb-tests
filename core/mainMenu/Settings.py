import os, sys
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../'))
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../..'))
from core.Menu import Menu
from settings.Video import Video
from core.mainMenu.settings.Audio import Audio
from core.mainMenu.settings.Display import Display
from core.mainMenu.settings.Language import Language
from core.mainMenu.settings.IPnetwork import IPNetwork
from core.mainMenu.settings.Environmental import Environmental
from core.mainMenu.settings.Favourites import Favourites
from core.mainMenu.settings.Recordings import Recordings
from core.mainMenu.settings.Multiview import Multiview
from core.mainMenu.settings.AdvancedSettings import AdvancedSettings
from environment.Plato import Plato
from environment.Resources import Resources

class Settings(Menu):

    def __init__(self):
        super(Menu, self).__init__()
        self.elementsList = ['Video', 'Audio', 'Language', 'Display', 'IPNetwork', 'Environmental', 'Favourites', 'ParentalControl', 'Recordings', 'Multiview', 'AdvancedSettings']
        self.setSelectedElement(self.elementsList[0])
        self.coordinatesDictionary = {'Video'           :(120,225,345,280),
                                      'Audio'           :(120,285,345,340),
                                      'Language'        :(120,360,345,415),
                                      'Display'         :(120,435,345,490),
                                      'IPNetwork'       :(120,510,345,565),
                                      'Environmental'   :(120,585,345,640),
                                      'Favourites'      :(120,660,345,715),
                                      'ParentalControl' :(120,730,345,785),
                                      'Recordings'      :(120,800,345,855),
                                      'Multiview'       :(120,875,345,930),
                                      'AdvancedSettings':(120,945,345,1000)
                                      }
        self.templatePicturesPath = Resources().SETTINGS_TEMPLATES_DIR
        
    def exit(self):
        self.remote.pressButton('EXIT')
    
    def openSelected(self):
        self.remote.pressButton('OK')
        
        if self.selectedElement == 'Video':
            return Video()
        elif self.selectedElement == 'Audio':
            return Audio()
        elif self.selectedElement == 'Language':
            return Language()
        elif self.selectedElement == 'Display':
            return Display()
        elif self.selectedElement == 'IPNetwork':
            return IPNetwork()
        elif self.selectedElement == 'Environmental':
            return Environmental()
        elif self.selectedElement == 'Favourites':
            return Favourites()
        elif self.selectedElement == 'ParentalControl':
            return ParentalControl()
        elif self.selectedElement == 'Recordings':
            return Recordings()
        elif self.selectedElement == 'Multiview':
            return Multiview()
        elif self.selectedElement == 'AdvancedSettings':
            return AdvancedSettings()
        else:
            return None