import os, sys
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../'))
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../..'))
from core.mainMenu.settings.SubMenu import SubMenu
from environment.Resources import Resources
from environment.Plato import Plato

class TVSignal(SubMenu):

    def __init__(self):
        SubMenu.__init__(self)
        self.elementsList = ['TVSignalWindowHeader', 'ChannelIcon', 'Frequency']
        for i in range (0, 390):        # entered 390 number because there is 390 channels.
            self.elementsList.append(str(i))
        self.coordinatesDictionary = {'TVSignalWindowHeader': (592,215,851,253),
                                      'ChannelIcon': (100,100,200,200),
                                      'Frequency': (200,200,300,300)}
        self.templatePicturesPath = Resources().HELP_TEMPLATE_DIR
        self.setSelectedElement(self.elementsList[0])
        
    def confirmSelectedChannel(self):
        self.remote.pressButton('OK')
        
    def confirmChannel(self, channel):
        self.selectElement(channel)
        self.confirmSelectedChannel()
        
    def isOpened(self):
        if (self.isSelected('TVSignalWindowHeader')):
            return self.isChannelChanged()
        else:
            print "TV Signal is not opened"
            return False
        #return self.isSelected(self.selectedElement)
    
    def isChannelChanged(self):  
        if (self.isSelected('ChannelIcon') and self.isSelected('Frequency')):
            return True
        else :
            print "Channel didn't change"
            return False