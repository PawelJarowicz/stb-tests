import os, sys
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../'))
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../..'))
from core.Menu import Menu
from environment.Remote import Remote
from core.mainMenu.help.FAQ import FAQ
from core.mainMenu.help.TVSignal import TVSignal
from core.mainMenu.help.IPNetwork import IPNetwork
from core.mainMenu.help.CA import CA
from core.mainMenu.help.Licensing import Licensing
from core.mainMenu.help.Reset import Reset
from core.mainMenu.help.Tuning import Tuning
from core.mainMenu.help.FactoryReset import FactoryReset
from core.mainMenu.help.SoftwareUpgrade import SoftwareUpgrade
from core.mainMenu.help.Environmental import Environmental
from core.mainMenu.help.Information import Information
from environment.Camera import Camera
from environment.Camera import CaptureError
from environment.Plato import Plato
from environment.Resources import Resources
    

class Help(Menu):
    

    def __init__(self):
        super(Help, self).__init__()
        self.elementsList = ['FAQ', 'TVSignal', 'IPNetwork', 'CA', 'Licensing', 'Reset', 'Tuning', 'FactoryReset', 'SoftwareUpgrade', 'Environmental', 'Information']
        self.setSelectedElement(self.elementsList[0])
        self.templatePicturesPath = Resources().HELP_TEMPLATE_DIR
        self.coordinatesDictionary = {
            'FAQ': (120,225,345,280),
            'TVSignal': (120,295,345,340),
            'IPNetwork': (120,360,345,415),
            'CA': (120,435,345,490),
            'Licensing': (120,510,345,565),
            'Reset': (120,585,345,640),
            'Tuning': (125,655,345,705),
            'FactoryReset': (125,730,355,770),
            'SoftwareUpgrade': (125,800,435,845),
            'Environmental': (120,875,380,915),
            'Information': (120,945,337,983)
        }
        
    def exit(self):
        self.__remote.pressButton('EXIT')
    
    def openSelected(self):
        self.remote.pressButton('OK')
        
        if self.selectedElement == 'FAQ':
            return FAQ()
        elif self.selectedElement == 'TVSignal':
            return TVSignal()
        elif self.selectedElement == 'IPNetwork':
            return IPNetwork()
        elif self.selectedElement == 'CA':
            return CA()
        elif self.selectedElement == 'Licensing':
            return Licensing()
        elif self.selectedElement == 'Reset':
            return Reset()
        elif self.selectedElement == 'Tuning':
            return Tuning()
        elif self.selectedElement == 'FactoryReset':
            return FactoryReset()
        elif self.selectedElement == 'SoftwareUpgrade':
            return SoftwareUpgrade()
        elif self.selectedElement == 'Environmental':
            return Environmental()
        elif self.selectedElement == 'Information':
            return Information()
        else:
            return None