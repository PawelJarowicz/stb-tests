import os, sys
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../'))
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../..'))
from environment.Remote import Remote
from core.LiveTV import LiveTV
from environment.Plato import Plato

class TVGuide(object):

    def __init__(self):
        self.__eventNumber = 0
        self.__channelNumber = 0
        self.__recordingsList = []
        self.__remindersList = []
        self.remote = Plato.remote
        
    def exitToLiveTV(self):
        self.remote.pressButton('EXIT')
        return LiveTV()
        
    def nextEvent(self):
        self.remote.pressButton('ARROW_RIGHT')
        self.__eventNumber = self.__eventNumber + 1
        
    def previousEvent(self):
        self.remote.pressButton('ARROW_LEFT')
        if self.__eventNumber > 0:
            self.__eventNumber = self.__eventNumber - 1
            
    def nextChannel(self):
        self.remote.pressButton('ARROW_DOWN')
        self.__channelNumber = self.__channelNumber + 1
        self.__eventNumber = 0
        
    def previousChannel(self):
        self.remote.pressButton('ARROW_LEFT')
        if self.__channelNumber > 0:
            self.__channelNumber = self.__channelNumber - 1
        self.__eventNumber = 0
     
    def nextDay(self):
        self.remote.pressButton('REWIND')
        
    def previousDay(self):
        self.remote.pressButton('FAST_FORWARD')
       
    def tuneToSelectedChannel(self):
        while self.__eventNumber > 0:
            self.previousEvent()
        self.remote.pressButton('OK')
        return LiveTV()
    
    def startRecording(self):
        if self.__recordingsList.count((self.__eventNumber, self.__channelNumber)) == 0:
            self.remote.pressButton('EXTRA')
            self.remote.pressButton('ARROW_DOWN')
            self.remote.pressButton('OK')
            self.__recordingsList.append((self.__eventNumber, self.__channelNumber))
            
    def stopRecording(self):
        if self.__recordingsList.count((self.__eventNumber, self.__channelNumber)) > 0:
            self.remote.pressButton('EXTRA')
            self.remote.pressButton('ARROW_DOWN')
            self.remote.pressButton('OK')
            self.__recordingsList.remove((self.__eventNumber, self.__channelNumber))
            
    def addReminder(self):
        if self.__remindersList.count((self.__eventNumber, self.__channelNumber)) == 0:
            self.remote.pressButton('EXTRA')
            self.remote.pressButton('OK')
            self.__remindersList.append((self.__eventNumber, self.__channelNumber))
            
    def removeReminder(self):
        if self.__remindersList.count((self.__eventNumber, self.__channelNumber)) > 0:
            self.remote.pressButton('EXTRA')
            self.remote.pressButton('OK')
            self.__remindersList.remove((self.__eventNumber, self.__channelNumber))